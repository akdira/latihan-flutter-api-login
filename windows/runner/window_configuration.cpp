#include "window_configuration.h"

const wchar_t* kFlutterWindowTitle = L"latihan_api_login";
const unsigned int kFlutterWindowOriginX = 10;
const unsigned int kFlutterWindowOriginY = 10;
const unsigned int kFlutterWindowWidth = 800;
const unsigned int kFlutterWindowHeight = 600;
