import 'package:flutter/material.dart';

class HomeIndexFragment extends StatefulWidget {  

  @override
  _HomeIndexFragmentState createState() => _HomeIndexFragmentState();
}

class _HomeIndexFragmentState extends State<HomeIndexFragment> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(Icons.home, size: 45,),
            Text('Mengenal API'),
          ],
        ),
      ),
    );
  }
}
