import 'dart:io';

import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:latihan_api_login/entities/DrawerItem.dart';
import 'package:latihan_api_login/ui/Default/Dictionary/DictionaryIndexFragment.dart';
import 'package:latihan_api_login/ui/Default/Home/HomeIndexFragment.dart';

class DefaultPage extends StatefulWidget {
    
  var listMenuItems = [
      new DrawerItem("Home", Icons.home),
      new DrawerItem("Dictionary", Icons.book),
      new DrawerItem("Logout", Icons.exit_to_app),
      new DrawerItem("Exit", Icons.close)
    ];

  @override
  State<StatefulWidget> createState() {
    return new DefaultPageState();
  }
}

class DefaultPageState extends State<DefaultPage> {

  int _selectedMenuIndex = 0;
  var loggedinEmail = '';
  var loggedinName = '';

  // Perintah yang dieksekusi ketika menu dipilih
  dynamic _getMenuItemWidget(int pos) {
    switch (pos) {
      case 0:
        return new HomeIndexFragment();
      case 1:
        return new DictionaryIndexFragment();

      // Case Logout
      case 2:

        // Kembali ke halaman paling awal
        Navigator.of(context, rootNavigator: true).pop(context);
        return new Scaffold();
      default:
        SystemChannels.platform.invokeMethod('SystemNavigator.pop');
        exit(0);
        return new Scaffold();
    }
  }

  // Detail tentang login di menu
  var accountDetailsOnMenu = 
    new UserAccountsDrawerHeader(
      accountEmail: Text('jerrysibarani@gmail.com'),
      accountName: Text('Jerry Sibarani'), 
      currentAccountPicture: CircleAvatar(child: new Icon(Icons.person, size: 50,)),
    );

  // Fungsi ketika menu dipilih
  void _onMenuSelected(int index) {
    setState(() => _selectedMenuIndex = index); // Mengubah variable yang berhubungan dengan menu index
    Navigator.of(context).pop(); // close the drawer
  }

  @override
  Widget build(BuildContext context) {

    // Generate menu berdasarkan list
    List<Widget> listMenuWidgets = [];
    for (var i = 0; i < widget.listMenuItems.length; i++) {
      var d = widget.listMenuItems[i];
      listMenuWidgets.add(new ListTile(
        leading: new Icon(d.icon),
        title: new Text(d.title),
        selected: i == _selectedMenuIndex,
        onTap: () => _onMenuSelected(i),
      ));
    }

    return new Scaffold(
      appBar: new AppBar(
        
        // Menampilkan menu berdasarkan item yang dipilih
        title: new Text(widget.listMenuItems[_selectedMenuIndex].title), 
      ),
      drawer: new Drawer(
        child: new Column(
          children: <Widget>[
            accountDetailsOnMenu,

            // Isi menu berdasarkan listMenuWidgets 
            new Column(children: listMenuWidgets)
          ],
        ),
      ),
      // Isi body berdasarkan menu yang dipilih
      body: _getMenuItemWidget(_selectedMenuIndex),
    );
  }
}
