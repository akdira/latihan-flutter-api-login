import 'package:flutter/material.dart';
import 'package:latihan_api_login/helpers/MessageBoxHelper.dart';
import 'package:latihan_api_login/models/Mauth.dart';

class LoginPage extends StatefulWidget {
  
  // Memanggil EntryFormState dengan parameter
  @override
  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  
  // Declare variable
  final _formKey = GlobalKey<FormState>();

  // Declare controller
  var usernameCtrl = TextEditingController();
  var passwordCtrl = TextEditingController();  
  var _mauth = new Mauth();

  // Widget build
  @override
  Widget build(BuildContext context) {

    var _messageBoxHelper = new MessageBoxHelper(context: context);
    
    // Form input
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: new Container(
        padding: new EdgeInsets.all(20.0),
        child: new Form(
          key: _formKey,
          child: new ListView(
            children: <Widget>[

              new TextFormField(
                controller: usernameCtrl, // Disambungkan dengan controller tadi
                keyboardType: TextInputType.text,
                decoration: new InputDecoration(
                  labelText: 'Username'
                ),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Mohon input Username';
                  }
                  return null;
                },
              ),

              // Input Password
              new TextFormField(
                controller: passwordCtrl, // Disambungkan dengan controller tadi
                keyboardType: TextInputType.visiblePassword, // Tipe keyboard password
                obscureText: true, // Masking password
                decoration: new InputDecoration(
                  labelText: 'Password'
                ),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Mohon input Password';
                  }
                  return null;
                },
              ),


              // Submit button
              new Container(
                margin: new EdgeInsets.only(top: 20.0),
                child: new RaisedButton(
                  color: Colors.blue,
                  child: new Text(
                    'Login',
                    style: new TextStyle(color: Colors.white),
                  ),
                  onPressed: (){
                    
                    // Jika validasi form berhasil
                    if (_formKey.currentState.validate()) {
                      Future.microtask(() async {

                        // Validasi ke api
                        var loginResult = await _mauth.login(usernameCtrl.text, passwordCtrl.text);

                        // Jika berhasil
                        if(loginResult.status == true){

                          // Navigasi ke halaman home
                          Navigator.of(context).pushNamed("/DefaultPage");
                        }
                        // Jika gagal
                        else{
                          _messageBoxHelper.show("Login Gagal", loginResult.error);
                        }
                        
                      });
                    }
                  },
                ),
              )
            ],
          ),
        ),
      )
    );
  }
  
}
