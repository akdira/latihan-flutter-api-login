import 'dart:async';
import 'package:requests/requests.dart'; // Dependecies untuk Requests pengganti http
import 'package:latihan_api_login/configs/constants.dart';
import 'package:latihan_api_login/entities/Dictionary.dart'; 

// Class model dictionary
// Ini berfungsi untuk menghubungkan dari entity Dictionary ke Model Dictionary
// Di mana Model Dictionary akan terhubung ke API
class Mdictionary {

  // Select dictionary
  Future<Dictionary> selectById(int id) async {
    var url = apiUrlAddress + "index.php/myapi/kamus_api?id"+id.toString();

    final response = await Requests.get(url);
        
    // Mapping dari json ke entity list
    List<Dictionary> objectList;
    objectList=(response.json() as List).map((i) =>
              Dictionary.fromJson(i)).toList();

    // Return null jika data tidak ditemukan
    if(objectList[0] == null)
    {
      return null;
    }
    // Return object jika data ditemukan
    else 
    {
      return objectList[0];
    }  
  }

  // Insert dictionary ke:
  // http://jerrysibarani.com/androidserverapi/index.php/myapi/kamus_api
  Future<int> insert(Dictionary _object) async {
    var url = apiUrlAddress + "index.php/myapi/kamus_api";

    Requests.post(url, body: {
      "word": _object.word,
      "description": _object.description
    });

    return 1;
  }
  
  // Update dictionary ke:
  // http://jerrysibarani.com/androidserverapi/index.php/myapi/kamus_api
  Future<int> update(Dictionary _object) async {
    var url = apiUrlAddress + "index.php/myapi/kamus_api";
    Requests.put(url, body: {
      "id": _object.id.toString(),
      "word": _object.word,
      "description": _object.description
    });

    return 0;
  }

  // Delete dictionary
  Future<int> delete(int id) async {
    var url = apiUrlAddress + "index.php/myapi/kamus_api_delete";

    Requests.post(url, body: {
      'id': id
    });
    return 0;
  }

  // Get List dari:
  // http://jerrysibarani.com/androidserverapi/index.php/myapi/kamus_api
  Future<List<Dictionary>> getList() async {
    var url = apiUrlAddress + "index.php/myapi/kamus_api";
    final response = await Requests.get(url);
    
    // Mapping dari json ke entity list
    List<Dictionary> objectList;
    objectList=(response.json() as List).map((i) =>
              Dictionary.fromJson(i)).toList();

    return objectList;
  }

}