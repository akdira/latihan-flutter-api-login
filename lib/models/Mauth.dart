import 'dart:async';
import 'package:requests/requests.dart'; // Dependecies untuk Requests pengganti http

import 'package:latihan_api_login/entities/Auth.dart';
import 'package:latihan_api_login/configs/constants.dart';

// Class model auth
// Ini berfungsi untuk melakukan login maupun logout
class Mauth {
  
  // Fungsi Login
  Future<Auth> login(String username, String password) async {
    var url = apiUrlAddress + "index.php/myapi/Auth_api/login";

    var response = await Requests.post(url, body: {
      "username": username,
      "password": password
    });

    var _json = response.json();
    var _authResult = new Auth.fromMap(_json);

    return _authResult;

  }
  
  // Fungsi Logout
  Future<Auth> logout() async {
    var url = apiUrlAddress + "index.php/myapi/Auth_api/logout";

    var response = await Requests.get(url);

    var _json = response.json();
    var _authResult = new Auth.fromMap(_json);

    return _authResult;

  }
  
}