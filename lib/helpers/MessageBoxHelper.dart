
import 'package:flutter/material.dart';

class MessageBoxHelper{

  // Context
  BuildContext context;

  // Constructor
  MessageBoxHelper({this.context});
  
  // Fungsi untuk memunculkan message box
  show(String _title, String _content) {

    // Tombol message box
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () { 
        Navigator.pop(this.context, true); // Close and return true
      },
    );
    

    // Dialog 
    AlertDialog alert = AlertDialog(
      title: Text(_title),
      content: Text(_content),
      actions: [
        okButton,
      ],
    );

    // Munculkan dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}