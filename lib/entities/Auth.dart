class Auth {
  bool status;
  String error;
  
  // Constructor ini memungkinkan anda untuk 
  // Membuat class beserta isinya sekaligus
  Auth({this.status, this.error});

  // Fungsi untuk menerima dari format Map
  Auth.fromMap(Map<String, dynamic> map) {
    this.status = map['status'];
    this.error = map['error'];
  }

  // Fungsi untuk return ke format Map
  Map<String, dynamic> toMap() {
    return {
      'status': status,
      'error': error,
    };
  }

  // Fungsi konversi dari json ke entity
  static Auth fromJson(json) {
    var object = new Auth();
    object.status = json['status'];
    object.error = json['error'];

    return object;
  }
}