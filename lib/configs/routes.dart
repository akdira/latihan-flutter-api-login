import 'package:flutter/material.dart';

// Harap diperhatikan url pada import bergantung pada folder 
// dan nama yang anda buat masing-masing. 
import 'package:latihan_api_login/ui/Default/DefaultPage.dart';
import 'package:latihan_api_login/ui/Login/LoginPage.dart';

// Anda dapat menelusuri alur coding dengan 
// melakukan klik kanan Go to Definition (F12) 
// pada function di bawah:
final routesMap = {
  '/LoginPage':      (BuildContext context) => new LoginPage(),
  '/DefaultPage':    (BuildContext context) => new DefaultPage(),
};