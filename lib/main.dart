import 'package:flutter/material.dart';
import 'package:latihan_api_login/ui/Login/LoginPage.dart';
import 'configs/routes.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        title: 'NavigationDrawer Demo',
        debugShowCheckedModeBanner: false,
        theme: new ThemeData(
          primarySwatch: Colors.teal,
        ),
        // home: new HomePage(),
        home: new LoginPage(),
        routes: routesMap 
        );
  }
}
